<?php 

    include_once('includes/connection.php');
    if(isset($_POST['diaValor']) and isset($_POST['anoValor']) and isset($_POST['mesValor'])){
        $anoValor=$_POST['anoValor'];
        $mesValor=$_POST['mesValor'];
        $diaValor=$_POST['diaValor'];
        $data=''.$anoValor.'-'.$mesValor.'-'.$diaValor.'';
        $queryFinal = mysqli_query($conn, "
        SELECT  porto.nome as NomePorto 
        , final.idPorto as id
        , sum(if(final.idMes = 0, final.valor, 0))  AS AtualValue
        , sum(if(final.idMes = 1, final.valor, 0))  AS JanValue
        , sum(if(final.idMes = 2, final.valor, 0))  AS FebValue
        , sum(if(final.idMes = 3, final.valor, 0))  AS MarValue
        , sum(if(final.idMes = 4, final.valor, 0))  AS AprValue
        , sum(if(final.idMes = 5, final.valor, 0))  AS MayValue
        , sum(if(final.idMes = 6, final.valor, 0))  AS JunValue
        , sum(if(final.idMes = 7, final.valor, 0))  AS JulValue
        , sum(if(final.idMes = 8, final.valor, 0))  AS AugValue
        , sum(if(final.idMes = 9, final.valor, 0))  AS SepValue
        , sum(if(final.idMes = 10, final.valor, 0)) AS OctValue
        , sum(if(final.idMes = 11, final.valor, 0)) AS NovValue
        , sum(if(final.idMes = 12, final.valor, 0)) AS DecValue
        FROM final

        inner join  porto
        ON          porto.id = final.idPorto
        where data='$data'
        group by porto.nome

        order by porto.nome
        ");
    }else{
        $data=date('Y-m-d');
        $queryFinal = mysqli_query($conn, "
        SELECT  porto.nome as NomePorto 
        , final.idPorto as id
        , sum(if(final.idMes = 0, final.valor, 0))  AS AtualValue
        , sum(if(final.idMes = 1, final.valor, 0))  AS JanValue
        , sum(if(final.idMes = 2, final.valor, 0))  AS FebValue
        , sum(if(final.idMes = 3, final.valor, 0))  AS MarValue
        , sum(if(final.idMes = 4, final.valor, 0))  AS AprValue
        , sum(if(final.idMes = 5, final.valor, 0))  AS MayValue
        , sum(if(final.idMes = 6, final.valor, 0))  AS JunValue
        , sum(if(final.idMes = 7, final.valor, 0))  AS JulValue
        , sum(if(final.idMes = 8, final.valor, 0))  AS AugValue
        , sum(if(final.idMes = 9, final.valor, 0))  AS SepValue
        , sum(if(final.idMes = 10, final.valor, 0)) AS OctValue
        , sum(if(final.idMes = 11, final.valor, 0)) AS NovValue
        , sum(if(final.idMes = 12, final.valor, 0)) AS DecValue
        FROM final

        inner join  porto
        ON          porto.id = final.idPorto
        where data='$data'
        group by porto.nome

        order by porto.nome
        ");
    }

?>