<?php 

    include_once('includes/connection.php');
    if(isset($_POST['diaValor']) and isset($_POST['anoValor']) and isset($_POST['mesValor'])){
        $anoValor=$_POST['anoValor'];
        $mesValor=$_POST['mesValor'];
        $diaValor=$_POST['diaValor'];
        $data=''.$anoValor.'-'.$mesValor.'-'.$diaValor.'';
        $queryPremio = mysqli_query($conn, "
        SELECT  porto.nome as NomePorto 
        , portopremio.idPorto as id
        , sum(if(portopremio.idMes = 0, portopremio.premio, 0))  AS AtualValue
        , sum(if(portopremio.idMes = 1, portopremio.premio, 0))  AS JanValue
        , sum(if(portopremio.idMes = 2, portopremio.premio, 0))  AS FebValue
        , sum(if(portopremio.idMes = 3, portopremio.premio, 0))  AS MarValue
        , sum(if(portopremio.idMes = 4, portopremio.premio, 0))  AS AprValue
        , sum(if(portopremio.idMes = 5, portopremio.premio, 0))  AS MayValue
        , sum(if(portopremio.idMes = 6, portopremio.premio, 0))  AS JunValue
        , sum(if(portopremio.idMes = 7, portopremio.premio, 0))  AS JulValue
        , sum(if(portopremio.idMes = 8, portopremio.premio, 0))  AS AugValue
        , sum(if(portopremio.idMes = 9, portopremio.premio, 0))  AS SepValue
        , sum(if(portopremio.idMes = 10, portopremio.premio, 0)) AS OctValue
        , sum(if(portopremio.idMes = 11, portopremio.premio, 0)) AS NovValue
        , sum(if(portopremio.idMes = 12, portopremio.premio, 0)) AS DecValue
        FROM portopremio

        inner join  porto
        ON          porto.id = portopremio.idPorto
        where data='$data'
        group by porto.nome

        order by porto.nome
        ");
    }else{
        $data=date('Y-m-d');
        $queryPremio = mysqli_query($conn, "
        SELECT  porto.nome as NomePorto 
        , portopremio.idPorto as id
        , sum(if(portopremio.idMes = 0, portopremio.premio, 0))  AS AtualValue
        , sum(if(portopremio.idMes = 1, portopremio.premio, 0))  AS JanValue
        , sum(if(portopremio.idMes = 2, portopremio.premio, 0))  AS FebValue
        , sum(if(portopremio.idMes = 3, portopremio.premio, 0))  AS MarValue
        , sum(if(portopremio.idMes = 4, portopremio.premio, 0))  AS AprValue
        , sum(if(portopremio.idMes = 5, portopremio.premio, 0))  AS MayValue
        , sum(if(portopremio.idMes = 6, portopremio.premio, 0))  AS JunValue
        , sum(if(portopremio.idMes = 7, portopremio.premio, 0))  AS JulValue
        , sum(if(portopremio.idMes = 8, portopremio.premio, 0))  AS AugValue
        , sum(if(portopremio.idMes = 9, portopremio.premio, 0))  AS SepValue
        , sum(if(portopremio.idMes = 10, portopremio.premio, 0)) AS OctValue
        , sum(if(portopremio.idMes = 11, portopremio.premio, 0)) AS NovValue
        , sum(if(portopremio.idMes = 12, portopremio.premio, 0)) AS DecValue
        FROM portopremio

        inner join  porto
        ON          porto.id = portopremio.idPorto
        where data='$data'
        group by porto.nome

        order by porto.nome
        ");
    }

?>
