<?php 

    include_once('includes/connection.php');
    if(isset($_POST['diaValor']) and isset($_POST['anoValor']) and isset($_POST['mesValor'])){
        $anoValor=$_POST['anoValor'];
        $mesValor=$_POST['mesValor'];
        $diaValor=$_POST['diaValor'];
        $data=''.$anoValor.'-'.$mesValor.'-'.$diaValor.'';
        $queryCustoporto = mysqli_query($conn, "
        SELECT 	porto.nome as NomePorto 
        , custoporto.idPorto as id
        , sum(if(custoporto.idMes = 0, custoporto.valor, 0))  AS AtualValue
        , sum(if(custoporto.idMes = 1, custoporto.valor, 0))  AS JanValue
        , sum(if(custoporto.idMes = 2, custoporto.valor, 0))  AS FebValue
        , sum(if(custoporto.idMes = 3, custoporto.valor, 0))  AS MarValue
        , sum(if(custoporto.idMes = 4, custoporto.valor, 0))  AS AprValue
        , sum(if(custoporto.idMes = 5, custoporto.valor, 0))  AS MayValue
        , sum(if(custoporto.idMes = 6, custoporto.valor, 0))  AS JunValue
        , sum(if(custoporto.idMes = 7, custoporto.valor, 0))  AS JulValue
        , sum(if(custoporto.idMes = 8, custoporto.valor, 0))  AS AugValue
        , sum(if(custoporto.idMes = 9, custoporto.valor, 0))  AS SepValue
        , sum(if(custoporto.idMes = 10, custoporto.valor, 0)) AS OctValue
        , sum(if(custoporto.idMes = 11, custoporto.valor, 0)) AS NovValue
        , sum(if(custoporto.idMes = 12, custoporto.valor, 0)) AS DecValue
        FROM custoporto

        inner join  porto
        ON			porto.id = custoporto.idPorto
        where data='$data'
        group by porto.nome

        order by porto.nome

        ");
    }else{
        $data=date('Y-m-d');
        $queryCustoporto = mysqli_query($conn, "
        SELECT  porto.nome as NomePorto 
        , custoporto.idPorto as id
        , sum(if(custoporto.idMes = 0, custoporto.valor, 0))  AS AtualValue
        , sum(if(custoporto.idMes = 1, custoporto.valor, 0))  AS JanValue
        , sum(if(custoporto.idMes = 2, custoporto.valor, 0))  AS FebValue
        , sum(if(custoporto.idMes = 3, custoporto.valor, 0))  AS MarValue
        , sum(if(custoporto.idMes = 4, custoporto.valor, 0))  AS AprValue
        , sum(if(custoporto.idMes = 5, custoporto.valor, 0))  AS MayValue
        , sum(if(custoporto.idMes = 6, custoporto.valor, 0))  AS JunValue
        , sum(if(custoporto.idMes = 7, custoporto.valor, 0))  AS JulValue
        , sum(if(custoporto.idMes = 8, custoporto.valor, 0))  AS AugValue
        , sum(if(custoporto.idMes = 9, custoporto.valor, 0))  AS SepValue
        , sum(if(custoporto.idMes = 10, custoporto.valor, 0)) AS OctValue
        , sum(if(custoporto.idMes = 11, custoporto.valor, 0)) AS NovValue
        , sum(if(custoporto.idMes = 12, custoporto.valor, 0)) AS DecValue
        FROM custoporto

        inner join  porto
        ON          porto.id = custoporto.idPorto
        where data='$data'
        group by porto.nome
        ");
    }
        
?>