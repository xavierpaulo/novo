<?php 

    include_once('includes/connection.php');
    if(isset($_POST['diaValor']) and isset($_POST['anoValor']) and isset($_POST['mesValor'])){
        $anoValor=$_POST['anoValor'];
        $mesValor=$_POST['mesValor'];
        $diaValor=$_POST['diaValor'];
        $data=''.$anoValor.'-'.$mesValor.'-'.$diaValor.'';
        $queryTku = mysqli_query($conn, "
        SELECT  distancia.id as id 
        , concat(distancia.startDistance, '-', distancia.limitDistance) as distance
        , sum(if(tku.idMes = 0, tku.valor, 0))  AS AtualValue
        , sum(if(tku.idMes = 1, tku.valor, 0))  AS JanValue
        , sum(if(tku.idMes = 2, tku.valor, 0))  AS FebValue
        , sum(if(tku.idMes = 3, tku.valor, 0))  AS MarValue
        , sum(if(tku.idMes = 4, tku.valor, 0))  AS AprValue
        , sum(if(tku.idMes = 5, tku.valor, 0))  AS MayValue
        , sum(if(tku.idMes = 6, tku.valor, 0))  AS JunValue
        , sum(if(tku.idMes = 7, tku.valor, 0))  AS JulValue
        , sum(if(tku.idMes = 8, tku.valor, 0))  AS AugValue
        , sum(if(tku.idMes = 9, tku.valor, 0))  AS SepValue
        , sum(if(tku.idMes = 10, tku.valor, 0)) AS OctValue
        , sum(if(tku.idMes = 11, tku.valor, 0)) AS NovValue
        , sum(if(tku.idMes = 12, tku.valor, 0)) AS DecValue
        FROM tku

        inner join  distancia
        ON          distancia.id = tku.idDistancia
        where data='$data'
        group by distancia.id

        order by distancia.id
        ");
    }else{
        $data=date('Y-m-d');
        $queryTku = mysqli_query($conn, "
        SELECT  distancia.id as id 
        , concat(distancia.startDistance, '-', distancia.limitDistance) as distance
        , sum(if(tku.idMes = 0, tku.valor, 0))  AS AtualValue
        , sum(if(tku.idMes = 1, tku.valor, 0))  AS JanValue
        , sum(if(tku.idMes = 2, tku.valor, 0))  AS FebValue
        , sum(if(tku.idMes = 3, tku.valor, 0))  AS MarValue
        , sum(if(tku.idMes = 4, tku.valor, 0))  AS AprValue
        , sum(if(tku.idMes = 5, tku.valor, 0))  AS MayValue
        , sum(if(tku.idMes = 6, tku.valor, 0))  AS JunValue
        , sum(if(tku.idMes = 7, tku.valor, 0))  AS JulValue
        , sum(if(tku.idMes = 8, tku.valor, 0))  AS AugValue
        , sum(if(tku.idMes = 9, tku.valor, 0))  AS SepValue
        , sum(if(tku.idMes = 10, tku.valor, 0)) AS OctValue
        , sum(if(tku.idMes = 11, tku.valor, 0)) AS NovValue
        , sum(if(tku.idMes = 12, tku.valor, 0)) AS DecValue
        FROM tku

        inner join  distancia
        ON          distancia.id = tku.idDistancia
        where data='$data'
        group by distancia.id

        order by distancia.id
        ");
    }

?>
