<?php 
    include_once("includes/premioCode.php");
    include_once("includes/mesCode.php");
    include_once("includes/custoportoCode.php");
    include_once("includes/portoCode.php");
    include_once("includes/freteAtualCode.php");    
    include_once("includes/connection.php");
    include_once("includes/distanciaCode.php");
    include_once("includes/freteFuturoCode.php");
    include_once("includes/tkuCode.php");
    include_once("includes/barcacaCode.php");
    include_once("includes/dolarCode.php");
    include_once("includes/tkuChaoCode.php");
    include_once("includes/freteAtualChaoCode.php");
    include_once("includes/freteFuturoChaoCode.php");
    include_once("includes/chicagoCode.php");
?>

<style>
h5.estilo{
    margin-top:0;
    margin-bottom:.5rem;
    margin-bottom:.5rem;
    font-family:Comic Sans, Comic Sans MS, cursive;
    font-weight:500;
    line-height:1.2;
    font-size:1.5rem;


}
input.estilo{
    width: 60px;
    height: 23px;
    text-align: justify-all;
}
.hidden {
    display: none;
}

</style>

<br>

<script src="assets/js/pages/mask.js"></script>
<script src="assets/js/pages/checkBox.js"></script>
<div class="container">
    <div class="row">
        <div class="col-md-40">
            <div class="jumbotron">
                <h1 class="display-4">Tabelas</h1>
                <p class="lead">Tabel v.Alpha</p>
                <hr class="my-4">
                <div class="row">
                    <div class="col-md-8 text">
                        <form action="" method="POST">
                         <select required name="anoValor" id="anoValor">
                        <option value=""> Ano </option>
                            <?php
                            $datona=(int)date('Y');
                            for ($i=$datona-2; $i <$datona+2; $i++) { 
                                echo '<option value="'.$i.'">'.$i.' </option>';
                            }
                                
                             ?>
                             
                        </select>
                        <select required name="mesValor" id="mesValor">
                            <option value=""> Mes </option>
                            <?php
                            while($row = mysqli_fetch_array($queryMes)){
                                echo '<option value="'.$row['id'].'">'.utf8_encode($row['nome']).' </option> ';
                            } 
                             ?>
                        </select>
                   
                        
                        <select required name="diaValor" id="diaValor">
                            <option value=""> Dia </option>  
                        </select>
                        <input type="submit" class="btn btn-danger" value="Pesquisar" id="Pesquisar">
                        </form>
                    </div>
                </div>
                <div class="row">
                    <br>
                </div>
                <div class="row">
                    <div class="col-md-8 ">
                        <div id="msgSubmit" class="alert alert-primary hidden"> Tabela atualiza com sucesso, <b>aguarde a atualização da página</b>!</div>
                        <div id="msgSubmitErro" class="alert alert-primary hidden"> Erro ao atualizar tabela !</div>
                        <input type="hidden" id="dataTabelas" value="<?php echo($data);?>">
                        <div class="alert alert-primary col-md-8"> <?php

                        if(isset($data)){
                                echo("<b>Data Selecionada: ($data)</b>");
                            }else{
                                $data=date('Y-m-d');
                                echo("<b>Data Selecionada: </b>");
                            }    ?>
                                
                        </div>
                    </div>
                    

                </div>
                <div class="row">
                        <h5 class="estilo">Tabela Dólar</h5>
                </div>
                <div class="row">
                        <?php include_once("pages/dolar.php"); ?>
                </div>
                <div><hr class="my-4"></div>
                <div class="row">
                        <h5 class="estilo">Tabela Chicago</h5>
                </div>
                <div class="row">
                    <?php include_once("pages/chicago.php"); ?>
                </div>
                <div><hr class="my-4"></div>
                <div class="row">
                        <h5 class="estilo">Premio Portuário</h5>
                </div>
                <div class="row">
                    <?php include_once("pages/premio.php"); ?>                  
                </div>
                <div><hr class="my-4"></div>

                <div class="row">
                        <h5 class="estilo">Custo Portuário</h5>
                </div>
                <div class="row">
                    <?php include_once("pages/custoporto.php"); ?>
                </div>
                <div><hr class="my-4"></div>

                <div class="row">
                        <h5 class="estilo">Frete Atual</h5>
                </div>
                <div class="row">
                    <?php include_once("pages/freteAtual.php"); ?>
                </div>
                <div><hr class="my-4"></div>

                <div class="row">
                        <h5 class="estilo">Frete Futuro</h5>
                </div>
                <div class="row">
                    <?php include_once("pages/freteFuturo.php"); ?>
                </div>
                <div><hr class="my-4"></div>

                <h5 class="estilo">TKU</h5>
                <div class="row">
                    <?php include_once("pages/tku.php"); ?>
                </div>
                <div class="row">

                    <h5 class="estilo text-center">
                        Barcaça
                    </h5>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <input type="checkbox" name="checkRodoviaria" id="checkRodoviaria" value="checkRodoviaria">
                        <label for="dado_rodoviaria"><b>% na Rodoviaria</b></label>
                    </div>
                    <div class="col-md-6">
                        <input type="text" for="checkRodoviaria" id="dado_rodoviaria" name="dado_rodoviaria" class="estilo">
                        <label class="text-center"> 
                            <?php while($rowRodoviaria=mysqli_fetch_array($dadoRodoviaria)){
                                if($rowRodoviaria['rodoviaria']!=0){
                                    echo(str_replace('.',',',number_format($rowRodoviaria['rodoviaria'],2,'.',''))."%");
                                }else{
                                    echo("-");
                                }

                        } ?></label>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-3">
                        <input type="checkbox" id="checkColetado" value="checkColetado"name="checkColetado">
                        <label for="dado_coletado"><b>Dado Coletado</b></label>
                    </div>
                    <div class="col-md-6">
                        <input type="text" for="checkColetado" id="dado_coletado" name="dado_coletado" class="estilo">
                        <label class="text-center"> 
                            <?php while($rowColetado=mysqli_fetch_array($dadoColetado)){
                                if($rowColetado['coletado']!=0){
                                    echo(str_replace('.',',',$rowColetado['coletado']));
                                }else{
                                    echo("-");
                                }

                        } ?></label>
                    </div>
                </div>
                
              <div class="row">
                    <div class="col-md-3">
                        <label for=""><b>Dado Usado</b></label>
                    </div>
                    <div class="col-md-6">
                       <label for=""><?php while($row=mysqli_fetch_array($barcaca)){
                        echo(str_replace('.',',',$row['valor']));
                       } ?></label>
                       <input type="hidden" id="dadoValue" value="<?php while($result=mysqli_fetch_array($tkuAtual)){
                        echo($result['valor']);
                       } ?>">
                    </div>
                </div>

                <div><hr class="my-4"></div>
                <h5 class="estilo">Frete Atual Com estrada de chão</h5>
                <div class="row">
                    <?php include_once("pages/freteAtualChao.php"); ?>
                </div>

                 <div><hr class="my-4"></div>
                <h5 class="estilo">Frete Futuro Com estrada de chão</h5>
                <div class="row">
                    <?php include_once("pages/freteFuturoChao.php"); ?>
                </div>

                <div><hr class="my-4"></div>
                <h5 class="estilo">TKU com Estrada de Chão</h5>
                <div class="row">
                    <?php include_once("pages/tkuChao.php"); ?>
                </div>
                <div><hr class="my-4"></div>
                <h5 class="estilo">Tabela de Preços</h5>
                <div class="row">
                    <?php include_once("pages/finalTabela.php"); ?>
                </div>
                <div class="row">
                    <div class="col-md-3 text-center">
                        <button type="submit" class="btn btn-danger" id="Atualizar" >Atualizar Dados</button>
                    </div>
                    
                    

                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript"> 


$(function(){
            $('#mesValor').change(function(){
                if( $(this).val() ) {
                    $('#diaValor').hide();
                    
                    if($(this).val()==1 || $(this).val()==3 || $(this).val()==5 || $(this).val()==7 || $(this).val()==8 || $(this).val()==10 || $(this).val()==12 ){
                        var numDia=31;
                    }else if($(this).val()==4 || $(this).val()==6 ||$(this).val()==9 || $(this).val()==11){
                        var numDia=30;
                    }else{
                        var numDia=28;
                    }
                        var options = '<option value="">Dia</option>'; 
                        for (var i = 1; i <= numDia ; i++) {
                            options += '<option value="' + i + '">' + i + '</option>';

                        }   
                        $('#diaValor').html(options).show();
                            
                    
                    
                } else {
                    $('diaValor').html('<option value="">Dia</option>');
                }
            });
        });

	</script>
  