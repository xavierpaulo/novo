<script src="assets/js/pages/custoporto.js"></script>

<div class="col-md-10 ">
    <table class="table table-striped" id="tbleditavelporto">
        <thead class="thead-dark">
            <tr>
                <th class="text-center" scope="col">Cidade</th>
                <th class="text-center" scope="col">Atual</th>
                <th class="text-center" scope="col">Janeiro</th>
                <th class="text-center" scope="col">Fevereiro</th>
                <th class="text-center" scope="col">Março</th>
                <th class="text-center" scope="col">Abril</th>
                <th class="text-center" scope="col">Maio</th>
                <th class="text-center" scope="col">Junho</th>
                <th class="text-center" scope="col">Julho</th>

            </tr>
        </thead>
        <tbody style="background: #fff;">
            <?php if(isset($data)){ 
                    if(mysqli_num_rows($queryCustoporto) == 0 ){
                       while($rowCustoPorto = mysqli_fetch_array($queryPorto2)){
                        
                        $atualiza="insert into custoporto set valor=0,idMes=(select id from mes where id=1),idPorto=(select id from porto where id=".$rowCustoPorto['id']."), data='$data'";
                        mysqli_query($conn,$atualiza);
                        
            ?>
                <tr>
                    <td class="hidden" ><?php echo($rowCustoPorto['id']); ?></td>
                    <td class="text-center"><?php echo(utf8_encode($rowCustoPorto['nome']));  ?></td>
                    <td class="editavel text-center"><?php echo ("-"); ?></td>
                    <td class="editavel text-center"><?php echo ("-"); ?></td>
                    <td class="editavel text-center"><?php echo ("-"); ?></td>
                    <td class="editavel text-center"><?php echo ("-"); ?></td>
                    <td class="editavel text-center"><?php echo ("-"); ?></td>
                    <td class="editavel text-center"><?php echo ("-"); ?></td>
                    <td class="editavel text-center"><?php echo ("-"); ?></td>
                    <td class="editavel text-center"><?php echo ("-"); ?></td>
                </tr>
            <?php
                        }
                        
                    }else{

                    while($row = mysqli_fetch_array($queryCustoporto)){
                            
                        
            ?>
                <tr>
                    <td class="hidden" ><?php echo($row['id']); ?></td>
                    <td class="text-center"><?php echo(utf8_encode($row['NomePorto']));  ?></td>
                    <td class="editavel text-center"><?php if($row['AtualValue'] == 0) {echo('-'); } else { echo(str_replace('.',',',$row['AtualValue'])); } ?></td>
                    <td class="editavel text-center"><?php if($row['JanValue'] == 0) {echo('-'); } else { echo(str_replace('.',',',$row['JanValue'])); } ?></td>
                    <td class="editavel text-center"><?php if($row['FebValue'] == 0) {echo('-'); } else { echo(str_replace('.',',',$row['FebValue'])); } ?></td>
                    <td class="editavel text-center"><?php if($row['MarValue'] == 0) {echo('-'); } else { echo(str_replace('.',',',$row['MarValue'])); } ?></td>
                    <td class="editavel text-center"><?php if($row['AprValue'] == 0) {echo('-'); } else { echo(str_replace('.',',',$row['AprValue'])); } ?></td>
                    <td class="editavel text-center"><?php if($row['MayValue'] == 0) {echo('-'); } else { echo(str_replace('.',',',$row['MayValue'])); } ?></td>
                    <td class="editavel text-center"><?php if($row['JunValue'] == 0) {echo('-'); } else { echo(str_replace('.',',',$row['JunValue'])); } ?></td>
                    <td class="editavel text-center"><?php if($row['JulValue'] == 0) {echo('-'); } else { echo(str_replace('.',',',$row['JulValue'])); } ?></td>
                </tr>
            <?php
                        }
                    }
                }
            ?>
        </tbody>
    </table>
</div>
<script type="text/javascript">
    $(function(){
            $('#Atualizar').click(function(){
                $('#tbleditavelporto').find('tr').each(function () {
                    var $tds = $(this).find('td');                     
                    
                    var datinha=document.getElementById('dataTabelas').value;
                    var data = {
                        id: $tds.eq(0).text(),
                        atual: $tds.eq(2).text().replace('.','').replace(',','.'),
                        janeiro: $tds.eq(3).text().replace('.','').replace(',','.'),
                        fevereiro: $tds.eq(4).text().replace('.','').replace(',','.'),
                        marco: $tds.eq(5).text().replace('.','').replace(',','.'),
                        abril: $tds.eq(6).text().replace('.','').replace(',','.'),
                        maio: $tds.eq(7).text().replace('.','').replace(',','.'),
                        junho: $tds.eq(8).text().replace('.','').replace(',','.'),
                        julho: $tds.eq(9).text().replace('.','').replace(',','.'),
                        datinha:datinha 
                    };
                    
                    $.ajax({
                        url:'service/custoportoService.php',
                        type:'POST',
                        data: data,
                        success:function(data){
                            $("#msgSubmit").removeClass().addClass('alert alert-success');
                            $('#msgSubmit').delay(1000).fadeOut();
                            setTimeout(location.reload.bind(location), 2500);
                        }, error: function() {
                            $("#msgSubmitErro").removeClass().addClass('alert alert-danger');
                            $('#msgSubmitErro').delay(1000).fadeOut();
                        }
                    })
            })
        })
    });
</script>
