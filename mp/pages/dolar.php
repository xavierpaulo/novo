

<div class="cold-md-3">
	<table class="table table-striped" id="tbleditaveldolar">
		<thead class="thead-dark">
			<tr>
				<th class="text-center" scope="col">Mes</th>
                <th class="text-center" scope="col">Valor</th>
			</tr>
		</thead>
		<tbody style="background: #fff; ">
			<?php if(isset($data)){ 
                    if(mysqli_num_rows($queryDolar) == 0 ){
                       while($rowDolar = mysqli_fetch_array($queryMes2)){
                        $atualiza="insert into dolar(valor,idMes,data) values(0,".$rowDolar['id'].",'$data')";
                        mysqli_query($conn,$atualiza);
                        
            ?>
                <tr>
                    <td class="text-center"><?php echo(utf8_encode($rowDolar['nome']));  ?></td>
                    <td class="editavel text-center"><?php echo ("-"); ?></td>
                </tr>
            <?php
                        }
                        
                    }else{

                    while($row = mysqli_fetch_array($queryDolar)){
                            
                        
            ?>
                <tr>
                    <td class="text-center"><?php echo(utf8_encode($row['NomeMes']));  ?></td>
                    <td class="editavel text-center"><?php if($row['valor'] == 0) {echo('-'); } else { echo(str_replace('.',',',number_format($row['valor'],2,'.',''))); } ?></td>
                </tr>
            <?php
                        }
                        
                    }
                }
            ?>
		</tbody>
	</table>
    </div>
<div class="col-md-3">
    <label for=""><b>Anual:</b></label>
    <input type="text" class="estilo" id="anualValue">

    <label id="tdAnualValue" class="text-center"><?php while($rowAnual=mysqli_fetch_array($queryAnual)){
        if(mysqli_num_rows($queryAnual) == 1 ){
            echo(utf8_encode($rowAnual['anual']."%"));
        }
    }  ?></label>

</div>
    
<script type="text/javascript">
    /*$(document).ready(function(){
         $('#tdAnualValue').click(function(){
            var conteudoOriginal= $(this).text();
            var novoElemento= $('<input/>',{ type:'text', value:conteudoOriginal, class:'text-center', style: 'width: 48px; height: 23px;'});
            $(this).html(novoElemento.bind('blur keydown',function(e){
                var keyCode=e.which;
                var conteudoNovo= $(this).val();
                if(conteudoNovo==""){
                    conteudoNovo="-";
                }
                if(keyCode==13 && conteudoNovo !=conteudoOriginal){
                    $(this).parent().html(conteudoNovo);   
                    
                }else if(keyCode==27 || e.type== 'blur'){
                    $(this).parent().html(conteudoNovo);
                }
            }));
            $(this).children().select();
        })

})*/
    $(function(){
            $('#Atualizar').click(function(){
                    var $tds =  $('#tbleditaveldolar').find('tr').find('td');                
                    var anual=document.getElementById('anualValue').value;
                    var datinha=document.getElementById('dataTabelas').value;
                    var data = {
                        dolar: $tds.eq(1).text().replace(',','.'),
                        anual:anual,
                        datinha:datinha 
                    };
                    
                    $.ajax({
                        url:'service/dolarService.php',
                        type:'POST',
                        data: data,
                        success:function(data){
                            $("#msgSubmit").removeClass().addClass('alert alert-success');
                            $('#msgSubmit').delay(1000).fadeOut();
                            setTimeout(location.reload.bind(location), 2000);
                        }, error: function() {
                            $("#msgSubmitErro").removeClass().addClass('alert alert-danger');
                            $('#msgSubmitErro').delay(1000).fadeOut();
                        }
                    })
            })
    });
</script>