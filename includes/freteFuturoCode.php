<?php 
    include_once("includes/connection.php");
    if(isset($_POST['diaValor']) and isset($_POST['anoValor']) and isset($_POST['mesValor'])){
        $anoValor=$_POST['anoValor'];
        $mesValor=$_POST['mesValor'];
        $diaValor=$_POST['diaValor'];
        $data=''.$anoValor.'-'.$mesValor.'-'.$diaValor.'';
        $queryFreteFuturo=mysqli_query($conn,"
        SELECT  distancia.id as id,concat(distancia.startDistance, '-', distancia.limitDistance) as distance
        , sum(if(fretefuturo.idMes = 1, fretefuturo.valor, 0))  AS JanValue
        , sum(if(fretefuturo.idMes = 2, fretefuturo.valor, 0))  AS FebValue
        , sum(if(fretefuturo.idMes = 3, fretefuturo.valor, 0))  AS MarValue
        , sum(if(fretefuturo.idMes = 4, fretefuturo.valor, 0))  AS AprValue
        , sum(if(fretefuturo.idMes = 5, fretefuturo.valor, 0))  AS MayValue
        , sum(if(fretefuturo.idMes = 6, fretefuturo.valor, 0))  AS JunValue
        , sum(if(fretefuturo.idMes = 7, fretefuturo.valor, 0))  AS JulValue
        , sum(if(fretefuturo.idMes = 8, fretefuturo.valor, 0))  AS AugValue
        , sum(if(fretefuturo.idMes = 9, fretefuturo.valor, 0))  AS SepValue
        , sum(if(fretefuturo.idMes = 10, fretefuturo.valor, 0)) AS OctValue
        , sum(if(fretefuturo.idMes = 11, fretefuturo.valor, 0)) AS NovValue
        , sum(if(fretefuturo.idMes = 12, fretefuturo.valor, 0)) AS DecValue
        FROM fretefuturo

        inner join  distancia
        ON          distancia.id = fretefuturo.idDistancia
        where data='$data'
        group by distancia.id

        order by distancia.id
        ");

    
    }else{
        $data=date('Y-m-d');
        $queryFreteFuturo=mysqli_query($conn,"
                SELECT  distancia.id as id,concat(distancia.startDistance, '-', distancia.limitDistance) as distance
                , sum(if(fretefuturo.idMes = 1, fretefuturo.valor, 0))  AS JanValue
                , sum(if(fretefuturo.idMes = 2, fretefuturo.valor, 0))  AS FebValue
                , sum(if(fretefuturo.idMes = 3, fretefuturo.valor, 0))  AS MarValue
                , sum(if(fretefuturo.idMes = 4, fretefuturo.valor, 0))  AS AprValue
                , sum(if(fretefuturo.idMes = 5, fretefuturo.valor, 0))  AS MayValue
                , sum(if(fretefuturo.idMes = 6, fretefuturo.valor, 0))  AS JunValue
                , sum(if(fretefuturo.idMes = 7, fretefuturo.valor, 0))  AS JulValue
                , sum(if(fretefuturo.idMes = 8, fretefuturo.valor, 0))  AS AugValue
                , sum(if(fretefuturo.idMes = 9, fretefuturo.valor, 0))  AS SepValue
                , sum(if(fretefuturo.idMes = 10, fretefuturo.valor, 0)) AS OctValue
                , sum(if(fretefuturo.idMes = 11, fretefuturo.valor, 0)) AS NovValue
                , sum(if(fretefuturo.idMes = 12, fretefuturo.valor, 0)) AS DecValue
                FROM fretefuturo

                inner join  distancia
                ON          distancia.id = fretefuturo.idDistancia
                where data='$data'
                group by distancia.id

                order by distancia.id
        ");   
    }

 ?>