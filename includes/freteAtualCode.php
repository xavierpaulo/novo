<?php 
    include_once("includes/connection.php");
    if(isset($_POST['diaValor']) and isset($_POST['anoValor']) and isset($_POST['mesValor'])){
        $anoValor=$_POST['anoValor'];
        $mesValor=$_POST['mesValor'];
        $diaValor=$_POST['diaValor'];
        $data=''.$anoValor.'-'.$mesValor.'-'.$diaValor.'';
        $queryFreteAtual=mysqli_query($conn,"
            SELECT   distancia.id as id,concat(distancia.startDistance, '-', distancia.limitDistance) as distance
            , sum(if( freteatual.idfrete = 1, freteatual.valor , 0 )) as FirstFrete
            , sum(if( freteatual.idfrete = 1, freteatual.distanciaValue , 0 )) as FirstKm
            , sum(if( freteatual.idfrete = 2, freteatual.valor , 0 )) as SecondFrete
            , sum(if( freteatual.idfrete = 2, freteatual.distanciaValue , 0 )) as SecondKm
            , sum(if( freteatual.idfrete = 3, freteatual.valor , 0 )) as ThirdFrete
            , sum(if( freteatual.idfrete = 3, freteatual.distanciaValue , 0 )) as ThirdKm
            , sum(if( freteatual.idfrete = 4, freteatual.valor , 0 )) as FourthFrete
            , sum(if( freteatual.idfrete = 4, freteatual.distanciaValue , 0 )) as FourthKm
            , sum(if( freteatual.idfrete = 5, freteatual.valor , 0 )) as FifthFrete
            , sum(if( freteatual.idfrete = 5, freteatual.distanciaValue , 0 )) as FifthKm
            , round(avg(if( freteatual.valor != 0, freteatual.valor/freteatual.distanciaValue, null )),5) as mediaTKU
            FROM freteatual
            inner join  distancia
            ON distancia.id = freteatual.idDistancia
            where data='$data'
            group by distancia.id
           

        ");
    }else{
        $data=date('Y-m-d');
        $queryFreteAtual=mysqli_query($conn,"
            SELECT   distancia.id as id,concat(distancia.startDistance, '-', distancia.limitDistance) as distance
            , sum(if( freteatual.idfrete = 1, freteatual.valor , 0 )) as FirstFrete
            , sum(if( freteatual.idfrete = 1, freteatual.distanciaValue , 0 )) as FirstKm
            , sum(if( freteatual.idfrete = 2, freteatual.valor , 0 )) as SecondFrete
            , sum(if( freteatual.idfrete = 2, freteatual.distanciaValue , 0 )) as SecondKm
            , sum(if( freteatual.idfrete = 3, freteatual.valor , 0 )) as ThirdFrete
            , sum(if( freteatual.idfrete = 3, freteatual.distanciaValue , 0 )) as ThirdKm
            , sum(if( freteatual.idfrete = 4, freteatual.valor , 0 )) as FourthFrete
            , sum(if( freteatual.idfrete = 4, freteatual.distanciaValue , 0 )) as FourthKm
            , sum(if( freteatual.idfrete = 5, freteatual.valor , 0 )) as FifthFrete
            , sum(if( freteatual.idfrete = 5, freteatual.distanciaValue , 0 )) as FifthKm
            , round(avg(if( freteatual.valor != 0, freteatual.valor/freteatual.distanciaValue, null )),5) as mediaTKU
            FROM freteatual
            inner join  distancia
            ON distancia.id = freteatual.idDistancia
            where data='$data'
            group by distancia.id
           

        ");
    }


    
    
        
 ?>