<?php 

    include_once('includes/connection.php');
    if(isset($_POST['diaValor']) and isset($_POST['anoValor']) and isset($_POST['mesValor'])){
        $anoValor=$_POST['anoValor'];
        $mesValor=$_POST['mesValor'];
        $diaValor=$_POST['diaValor'];
        $data=''.$anoValor.'-'.$mesValor.'-'.$diaValor.'';
        $queryTkuChao = mysqli_query($conn, "
        SELECT  distancia.id as id 
        , concat(distancia.startDistance, '-', distancia.limitDistance) as distance
        , sum(if(tkuchao.idMes = 0, tkuchao.valor, 0))  AS AtualValue
        , sum(if(tkuchao.idMes = 1, tkuchao.valor, 0))  AS JanValue
        , sum(if(tkuchao.idMes = 2, tkuchao.valor, 0))  AS FebValue
        , sum(if(tkuchao.idMes = 3, tkuchao.valor, 0))  AS MarValue
        , sum(if(tkuchao.idMes = 4, tkuchao.valor, 0))  AS AprValue
        , sum(if(tkuchao.idMes = 5, tkuchao.valor, 0))  AS MayValue
        , sum(if(tkuchao.idMes = 6, tkuchao.valor, 0))  AS JunValue
        , sum(if(tkuchao.idMes = 7, tkuchao.valor, 0))  AS JulValue
        , sum(if(tkuchao.idMes = 8, tkuchao.valor, 0))  AS AugValue
        , sum(if(tkuchao.idMes = 9, tkuchao.valor, 0))  AS SepValue
        , sum(if(tkuchao.idMes = 10, tkuchao.valor, 0)) AS OctValue
        , sum(if(tkuchao.idMes = 11, tkuchao.valor, 0)) AS NovValue
        , sum(if(tkuchao.idMes = 12, tkuchao.valor, 0)) AS DecValue
        FROM tkuchao

        inner join  distancia
        ON          distancia.id = tkuchao.idDistancia
        where data='$data'
        group by distancia.id

        order by distancia.id
        ");
    }else{
        $data=date('Y-m-d');
        $queryTkuChao = mysqli_query($conn, "
        SELECT  distancia.id as id 
        , concat(distancia.startDistance, '-', distancia.limitDistance) as distance
        , sum(if(tkuchao.idMes = 0, tkuchao.valor, 0))  AS AtualValue
        , sum(if(tkuchao.idMes = 1, tkuchao.valor, 0))  AS JanValue
        , sum(if(tkuchao.idMes = 2, tkuchao.valor, 0))  AS FebValue
        , sum(if(tkuchao.idMes = 3, tkuchao.valor, 0))  AS MarValue
        , sum(if(tkuchao.idMes = 4, tkuchao.valor, 0))  AS AprValue
        , sum(if(tkuchao.idMes = 5, tkuchao.valor, 0))  AS MayValue
        , sum(if(tkuchao.idMes = 6, tkuchao.valor, 0))  AS JunValue
        , sum(if(tkuchao.idMes = 7, tkuchao.valor, 0))  AS JulValue
        , sum(if(tkuchao.idMes = 8, tkuchao.valor, 0))  AS AugValue
        , sum(if(tkuchao.idMes = 9, tkuchao.valor, 0))  AS SepValue
        , sum(if(tkuchao.idMes = 10, tkuchao.valor, 0)) AS OctValue
        , sum(if(tkuchao.idMes = 11, tkuchao.valor, 0)) AS NovValue
        , sum(if(tkuchao.idMes = 12, tkuchao.valor, 0)) AS DecValue
        FROM tkuchao

        inner join  distancia
        ON          distancia.id = tkuchao.idDistancia
        where data='$data'
        group by distancia.id

        order by distancia.id
        ");
    }

?>
