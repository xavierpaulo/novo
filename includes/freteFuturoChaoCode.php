<?php 
    include_once("includes/connection.php");
    if(isset($_POST['diaValor']) and isset($_POST['anoValor']) and isset($_POST['mesValor'])){
        $anoValor=$_POST['anoValor'];
        $mesValor=$_POST['mesValor'];
        $diaValor=$_POST['diaValor'];
        $data=''.$anoValor.'-'.$mesValor.'-'.$diaValor.'';
        $queryFreteFuturoChao=mysqli_query($conn,"
        SELECT  distancia.id as id,concat(distancia.startDistance, '-', distancia.limitDistance) as distance
        , sum(if(fretefuturochao.idMes = 1, fretefuturochao.valor, 0))  AS JanValue
        , sum(if(fretefuturochao.idMes = 2, fretefuturochao.valor, 0))  AS FebValue
        , sum(if(fretefuturochao.idMes = 3, fretefuturochao.valor, 0))  AS MarValue
        , sum(if(fretefuturochao.idMes = 4, fretefuturochao.valor, 0))  AS AprValue
        , sum(if(fretefuturochao.idMes = 5, fretefuturochao.valor, 0))  AS MayValue
        , sum(if(fretefuturochao.idMes = 6, fretefuturochao.valor, 0))  AS JunValue
        , sum(if(fretefuturochao.idMes = 7, fretefuturochao.valor, 0))  AS JulValue
        , sum(if(fretefuturochao.idMes = 8, fretefuturochao.valor, 0))  AS AugValue
        , sum(if(fretefuturochao.idMes = 9, fretefuturochao.valor, 0))  AS SepValue
        , sum(if(fretefuturochao.idMes = 10, fretefuturochao.valor, 0)) AS OctValue
        , sum(if(fretefuturochao.idMes = 11, fretefuturochao.valor, 0)) AS NovValue
        , sum(if(fretefuturochao.idMes = 12, fretefuturochao.valor, 0)) AS DecValue
        FROM fretefuturochao

        inner join  distancia
        ON          distancia.id = fretefuturochao.idDistancia
        where data='$data'
        group by distancia.id

        order by distancia.id
        ");

    
    }else{
        $data=date('Y-m-d');
        $queryFreteFuturoChao=mysqli_query($conn,"
                SELECT  distancia.id as id,concat(distancia.startDistance, '-', distancia.limitDistance) as distance
                , sum(if(fretefuturochao.idMes = 1, fretefuturochao.valor, 0))  AS JanValue
                , sum(if(fretefuturochao.idMes = 2, fretefuturochao.valor, 0))  AS FebValue
                , sum(if(fretefuturochao.idMes = 3, fretefuturochao.valor, 0))  AS MarValue
                , sum(if(fretefuturochao.idMes = 4, fretefuturochao.valor, 0))  AS AprValue
                , sum(if(fretefuturochao.idMes = 5, fretefuturochao.valor, 0))  AS MayValue
                , sum(if(fretefuturochao.idMes = 6, fretefuturochao.valor, 0))  AS JunValue
                , sum(if(fretefuturochao.idMes = 7, fretefuturochao.valor, 0))  AS JulValue
                , sum(if(fretefuturochao.idMes = 8, fretefuturochao.valor, 0))  AS AugValue
                , sum(if(fretefuturochao.idMes = 9, fretefuturochao.valor, 0))  AS SepValue
                , sum(if(fretefuturochao.idMes = 10, fretefuturochao.valor, 0)) AS OctValue
                , sum(if(fretefuturochao.idMes = 11, fretefuturochao.valor, 0)) AS NovValue
                , sum(if(fretefuturochao.idMes = 12, fretefuturochao.valor, 0)) AS DecValue
                FROM fretefuturochao

                inner join  distancia
                ON          distancia.id = fretefuturochao.idDistancia
                where data='$data'
                group by distancia.id

                order by distancia.id
        ");   
    }

 ?>