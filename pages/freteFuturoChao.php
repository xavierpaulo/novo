<script src="assets/js/pages/freteFuturoChao.js"></script>

<div class="col-md-10 ">
    <table class="table table-striped" id="tbleditavelfretefuturochao">
        <thead class="thead-dark">
            <tr>
                <th class="text-center" scope="col">Distância</th>
                <th class="text-center" scope="col">Janeiro</th>
                <th class="text-center" scope="col">Fevereiro</th>
                <th class="text-center" scope="col">Março</th>
                <th class="text-center" scope="col">Abril</th>
                <th class="text-center" scope="col">Maio</th>
                <th class="text-center" scope="col">Junho</th>
                <th class="text-center" scope="col">Julho</th>

            </tr>
        </thead>
        <tbody style="background: #fff;">
            <?php if(isset($data)){ 
                    if(mysqli_num_rows($queryFreteFuturoChao) == 0 ){
                       while($rowFreteFuturoChao = mysqli_fetch_array($queryDistancia6)){
                        
                        $atualiza="insert into fretefuturochao(valor,idMes,idDistancia,data) values(0, 1,".$rowFreteFuturoChao['id']." , '$data')";
                        mysqli_query($conn,$atualiza);
                        
            ?>
                <tr>
                    <td class="hidden" ><?php echo($rowFreteFuturoChao['id']); ?></td>
                    <td class="text-center"><?php 
                    if($rowFreteFuturoChao['startDistance']==2001){
                        echo("2001+");
                    }else{
                        echo(utf8_encode("".$rowFreteFuturoChao['startDistance']."-".$rowFreteFuturoChao['limitDistance']." ")); 
                    }
                    ?></td>
                    <td class="editavel text-center"><?php echo ("-"); ?></td>
                    <td class="editavel text-center"><?php echo ("-"); ?></td>
                    <td class="editavel text-center"><?php echo ("-"); ?></td>
                    <td class="editavel text-center"><?php echo ("-"); ?></td>
                    <td class="editavel text-center"><?php echo ("-"); ?></td>
                    <td class="editavel text-center"><?php echo ("-"); ?></td>
                    <td class="editavel text-center"><?php echo ("-"); ?></td>

                </tr>
            <?php
                        }
                        
                    }else{

                    while($row = mysqli_fetch_array($queryFreteFuturoChao)){
                            
                        
            ?>
                <tr>
                    <td class="hidden" ><?php echo($row['id']); ?></td>
                    <td class="text-center"><?php if($row['id']==6){
                        echo("2001+");
                    }else{echo(utf8_encode(str_replace('.',',',$row['distance'])));
                    }  ?></td>
                    <td class="editavel text-center"><?php if($row['JanValue'] == 0) {echo('-'); } else { echo(str_replace('.',',',$row['JanValue'])); } ?></td>
                    <td class="editavel text-center"><?php if($row['FebValue'] == 0) {echo('-'); } else { echo(str_replace('.',',',$row['FebValue'])); } ?></td>
                    <td class="editavel text-center"><?php if($row['MarValue'] == 0) {echo('-'); } else { echo(str_replace('.',',',$row['MarValue'])); } ?></td>
                    <td class="editavel text-center"><?php if($row['AprValue'] == 0) {echo('-'); } else { echo(str_replace('.',',',$row['AprValue'])); } ?></td>
                    <td class="editavel text-center"><?php if($row['MayValue'] == 0) {echo('-'); } else { echo(str_replace('.',',',$row['MayValue'])); } ?></td>
                    <td class="editavel text-center"><?php if($row['JunValue'] == 0) {echo('-'); } else { echo(str_replace('.',',',$row['JunValue'])); } ?></td>
                    <td class="editavel text-center"><?php if($row['JulValue'] == 0) {echo('-'); } else { echo(str_replace('.',',',$row['JulValue'])); } ?></td>
                </tr>
            <?php
                        }
                    }
                }
            ?>
        </tbody>
    </table>
</div>
<script type="text/javascript">
    $(function(){
            $('#Atualizar').click(function(){
                $('#tbleditavelfretefuturochao').find('tr').each(function () {
                    var $tds = $(this).find('td');                     
                    
                    var datinha=document.getElementById('dataTabelas').value;
                    var data = {
                        id: $tds.eq(0).text(),
                        janeiro: $tds.eq(2).text().replace(',','.'),
                        fevereiro: $tds.eq(3).text().replace(',','.'),
                        marco: $tds.eq(4).text().replace(',','.'),
                        abril: $tds.eq(5).text().replace(',','.'),
                        maio: $tds.eq(6).text().replace(',','.'),
                        junho: $tds.eq(7).text().replace(',','.'),
                        julho: $tds.eq(8).text().replace(',','.'),
                        datinha:datinha 

                    };
                    
                    $.ajax({
                        url:'service/freteFuturoChaoService.php',
                        type:'POST',
                        data: data,
                        success:function(data){
                            $("#msgSubmit").removeClass().addClass('alert alert-success');
                            $('#msgSubmit').delay(1000).fadeOut();
                            setTimeout(location.reload.bind(location), 2000);
                        }, error: function() {
                            $("#msgSubmitErro").removeClass().addClass('alert alert-danger');
                            $('#msgSubmitErro').delay(1000).fadeOut();
                        }
                    })
            })
        })
    });
</script>