<script src="assets/js/pages/tkuchao.js"></script>

<div class="col-md-10">
    <table class="table table-striped" id="tbleditaveltkuchao">
          <thead class="thead-dark">
            <tr>
                <th class="text-center" scope="col">Distancia</th>
                <th class="text-center" scope="col">Atual</th>
                <th class="text-center" scope="col">Janeiro</th>
                <th class="text-center" scope="col">Fevereiro</th>
                <th class="text-center" scope="col">Março</th>
                <th class="text-center" scope="col">Abril</th>
                <th class="text-center" scope="col">Maio</th>
                <th class="text-center" scope="col">Junho</th>
                <th class="text-center" scope="col">Julho</th>
            </tr>
        </thead>
        <tbody style="background: #fff;">
            <?php if(isset($data)){ 
                    if(mysqli_num_rows($queryTkuChao) ==0){
                       while($rowDistancia = mysqli_fetch_array($queryDistancia4)){
                        $atualiza="insert into tkuchao(idMes,valor,idDistancia,data) values(1, 0 , ".$rowDistancia['id']." , '$data')";
                        mysqli_query($conn,$atualiza);
                        
            ?>
                <tr>
                    <td class="hidden" ><?php echo($rowDistancia['id']); ?></td>
                    <td class="text-center"><?php 
                    if($rowDistancia['startDistance']==2001){
                        echo("2001+");
                    }else{
                        echo(utf8_encode("".$rowDistancia['startDistance']."-".$rowDistancia['limitDistance']." ")); 
                    }
                    ?></td>
                    <td class=" text-center"><?php echo ("-"); ?></td>
                    <td class="editavel text-center"><?php echo ("-"); ?></td>
                    <td class="editavel text-center"><?php echo ("-"); ?></td>
                    <td class="editavel text-center"><?php echo ("-"); ?></td>
                    <td class="editavel text-center"><?php echo ("-"); ?></td>
                    <td class="editavel text-center"><?php echo ("-"); ?></td> 
                    <td class="editavel text-center"><?php echo ("-"); ?></td>
                    <td class="editavel text-center"><?php echo ("-"); ?></td>
                </tr>
            <?php
                        }
                        
                    }else{

                    while($row = mysqli_fetch_array($queryTkuChao)){
            ?>
                <tr>
                    <td class="hidden" ><?php echo($row['id']); ?></td>
                    <td class="text-center"><?php 
                    if($row['id']==6){
                        echo("2001+");
                    }else{
                        echo(utf8_encode(str_replace('.',',',$row['distance']))); 
                    }
                    ?></td>
                    <td class=" text-center"><?php if($row['AtualValue'] == 0) {echo('-'); } else { echo(str_replace('.',',',$row['AtualValue'])); } ?></td>
                    <td class="editavel text-center"><?php if($row['JanValue'] == 0) {echo('-'); } else { echo(str_replace('.',',',$row['JanValue'])); } ?></td>
                    <td class="editavel text-center"><?php if($row['FebValue'] == 0) {echo('-'); } else { echo(str_replace('.',',',$row['FebValue'])); } ?></td>
                    <td class="editavel text-center"><?php if($row['MarValue'] == 0) {echo('-'); } else { echo(str_replace('.',',',$row['MarValue'])); } ?></td>
                    <td class="editavel text-center"><?php if($row['AprValue'] == 0) {echo('-'); } else { echo(str_replace('.',',',$row['AprValue'])); } ?></td>
                    <td class="editavel text-center"><?php if($row['MayValue'] == 0) {echo('-'); } else { echo(str_replace('.',',',$row['MayValue'])); } ?></td>
                    <td class="editavel text-center"><?php if($row['JunValue'] == 0) {echo('-'); } else { echo(str_replace('.',',',$row['JunValue'])); } ?></td>
                    <td class="editavel text-center"><?php if($row['JulValue'] == 0) {echo('-'); } else { echo(str_replace('.',',',$row['JulValue'])); } ?></td>
                </tr>
            <?php
                        }
                    }
                }
            ?>
        </tbody>
    </table>
</div>
 <script type="text/javascript">
    $(function(){
            $('#Atualizar').click(function(){
                $('#tbleditaveltkuchao').find('tr').each(function () {
                    var $tds = $(this).find('td');                     
                    
                    var datinha=document.getElementById('dataTabelas').value;
                    var data = {
                        id: $tds.eq(0).text(),
                        atual: $tds.eq(2).text().replace(',','.'),
                        janeiro: $tds.eq(3).text().replace(',','.'),
                        fevereiro: $tds.eq(4).text().replace(',','.'),
                        marco: $tds.eq(5).text().replace(',','.'),
                        abril: $tds.eq(6).text().replace(',','.'),
                        maio: $tds.eq(7).text().replace(',','.'),
                        junho: $tds.eq(8).text().replace(',','.'),
                        julho: $tds.eq(9).text().replace(',','.'),
                        datinha:datinha 
                    };
                    
                    $.ajax({
                        url:'service/tkuChaoService.php',
                        type:'POST',
                        data: data,
                        success:function(data){
                            $("#msgSubmit").removeClass().addClass('alert alert-success');
                            $('#msgSubmit').delay(1000).fadeOut();
                            setTimeout(location.reload.bind(location), 0);
                        }, error: function() {
                            $("#msgSubmitErro").removeClass().addClass('alert alert-danger');
                            $('#msgSubmitErro').delay(1000).fadeOut();
                        }
                    })
            })
        })
    });
</script>