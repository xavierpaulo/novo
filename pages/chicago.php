
<script src="assets/js/pages/chicago.js"></script>
<div class="cold-md-4">
	<table class="table table-striped" id="tbleditavelchicago">
		<thead class="thead-dark">
			<tr>
				<th class="text-center" scope="col">Mes</th>
                <th class="text-center" scope="col">Valor</th>
			</tr>
		</thead>
		<tbody style="background: #fff; ">
			<?php if(isset($data)){ 
                    if(mysqli_num_rows($queryChicago) == 0 ){
                       while($rowChicago = mysqli_fetch_array($queryMes3)){
                        $atualiza="insert into chicago(valor,idMes,data) values(0,".$rowChicago['id'].",'$data')";
                        mysqli_query($conn,$atualiza);
                        
            ?>
                <tr>
                    <td class="hidden" ><?php echo($rowChicago['id']); ?></td>
                    <td class="text-center"><?php echo(utf8_encode($rowChicago['nome']));  ?></td>
                    <td class="editavel text-center"><?php echo ("-"); ?></td>
                </tr>
            <?php
                        }
                        
                    }else{

                    while($row = mysqli_fetch_array($queryChicago)){
                            
                        
            ?>
                <tr>
                    <td class="hidden" ><?php echo($row['id']); ?></td>
                    <td class="text-center"><?php echo(utf8_encode($row['NomeMes']));  ?></td>
                    <td class="editavel text-center"><?php if($row['valor'] == 0) {echo('-'); } else { echo(str_replace('.',',',number_format($row['valor'],2,'.',''))); } ?></td>
                </tr>
            <?php
                        }
                    }
                }
            ?>
		</tbody>
	</table>
</div>
<script type="text/javascript">
            $(function(){
            $('#Atualizar').click(function(){
                $('#tbleditavelchicago').find('tr').each(function () {
                    var $tds = $(this).find('td');                     
                    var datinha=document.getElementById('dataTabelas').value;
                    var data = {
                        id: $tds.eq(0).text(),
                        valor: $tds.eq(2).text().replace(',','.'),
                        datinha:datinha 
                    };
                    
                    $.ajax({
                        url:'service/chicagoService.php',
                        type:'POST',
                        data: data,
                        success:function(data){
                            console.log(data);
                            $("#msgSubmit").removeClass().addClass('alert alert-success');
                            $('#msgSubmit').delay(1000).fadeOut();
                            setTimeout(location.reload.bind(location), 2500);
                        }, error: function() {
                            $("#msgSubmitErro").removeClass().addClass('alert alert-danger');
                            $('#msgSubmitErro').delay(1000).fadeOut();
                        }
                    })
            })
        })
    });
        </script>