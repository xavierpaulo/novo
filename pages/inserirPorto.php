<?php 
	include_once("../includes/connection.php");
	include_once("../includes/portoCode.php");
	 ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js" type="text/javascript"></script>
<script src="../assets/js/pages/checkBox.js"></script>
<html lang="pt-br">
<head>
	<meta charset="utf-8" />
	<title>Table Style</title>
	<meta name="viewport" content="initial-scale=1.0; maximum-scale=1.0; width=device-width;">
	<style type="text/css" media="screen"><!--
@import url("../assets/css/table.css");
--></style>
</head>

	 
	<body>
	<div class="table-title">
	<h3>Data Table</h3>
	</div>
	<div class="row">
		<div class="col-md-30">
	<table class="table-fill" id="checkBoxTable">
	<thead>
		<tr>
		<th class="text-left">ID</th>
		<th class="text-left">Porto</th>
		<th class="text-center"><input name="checkAll" class="checkAll" id="checkAll" type="checkbox"></th>
		</tr>
	</thead>
	<tbody class="table-hover">
		<?php while($row=mysqli_fetch_array($queryPorto)){	 ?>

		
			<tr>
			<td class="text-left"><?php echo($row['id']); ?></td>
			<td class="text-left"><?php echo(utf8_encode($row['nome'])); ?></td>
			<td class="text-center"><input type="checkBox" name="<?php echo($row['id']);?>"></td>
			</tr>
			<?php } ?>
	</tbody>
	</table>
	</div>
	<div class="row">
		<div class="col-md-8 text-left">
			<input type="submit" value="Pesquisar" id="Pesquisar">
		</div>
	

		
	</div>
</div>
  	
  	</body>