<script src="assets/js/pages/freteatual.js"></script>
<div class="col-md-12">
    <table class="table table-striped" id="tbleditavelfreteatual">
        <thead class="thead-dark">
            <tr>
                <th class="text-center" scope="col">Distância</th>
                <th class="text-center" scope="col">Frete 1</th>
                <th class="text-center" scope="col">KM1</th>
                <th class="text-center" scope="col">Frete 2</th>
                <th class="text-center" scope="col">KM2</th>
                <th class="text-center" scope="col">Frete 3</th>
                <th class="text-center" scope="col">KM3</th>
                <th class="text-center" scope="col">Frete 4</th>
                <th class="text-center" scope="col">KM4</th>
                <th class="text-center" scope="col">Frete 5</th>
                <th class="text-center" scope="col">KM5</th>
                <th class="text-center" scope="col">Media TKU</th>

            </tr>
        </thead>
        <tbody style="background: #fff;">
            <?php if(isset($data)){ 
                    if(mysqli_num_rows($queryFreteAtual) == 0 ){
                       while($rowDistancia = mysqli_fetch_array($queryDistancia)){
                        
                        $atualiza="insert into freteatual(idfrete,valor,distanciaValue,idDistancia,data) values(1, 0, 0 , ".$rowDistancia['id']." , '$data')";
                        mysqli_query($conn,$atualiza);
                        
            ?>
                <tr>
                    <td class="hidden" ><?php echo($rowDistancia['id']); ?></td>
                    <td class="text-center"><?php 
                    if($rowDistancia['startDistance']==2001){
                        echo("2001+");
                    }else{
                        echo(utf8_encode("".$rowDistancia['startDistance']."-".$rowDistancia['limitDistance']." ")); 
                    }
                    ?></td>
                    <td class="editavel text-center"><?php echo ("-"); ?></td>
                    <td class="editavel text-center"><?php echo ("-"); ?></td>
                    <td class="editavel text-center"><?php echo ("-"); ?></td>
                    <td class="editavel text-center"><?php echo ("-"); ?></td>
                    <td class="editavel text-center"><?php echo ("-"); ?></td>
                    <td class="editavel text-center"><?php echo ("-"); ?></td>
                    <td class="editavel text-center"><?php echo ("-"); ?></td>
                    <td class="editavel text-center"><?php echo ("-"); ?></td>
                    <td class="editavel text-center"><?php echo ("-"); ?></td>
                    <td class="editavel text-center"><?php echo ("-"); ?></td>
                    <td class="text-center"><?php echo ("-"); ?></td>

                </tr>
            <?php
                        }
                        
                    }else{

                    while($row = mysqli_fetch_array($queryFreteAtual)){
                            
                        
            ?>
                <tr>    
                    <td class="hidden" ><?php echo($row['id']); ?></td>
                    <td class="text-center"><?php if($row['id']==6){
                        echo("2001+");
                    }else{echo(utf8_encode(str_replace('.',',',$row['distance'])));
                    }  ?></td>
                    <td class="editavel text-center"><?php if($row['FirstFrete'] == 0) {echo('-'); } else { echo(str_replace('.',',',$row['FirstFrete'])); } ?></td>
                    <td class="editavel text-center"><?php if($row['FirstKm'] == 0) {echo('-'); } else { echo("".$row['FirstKm']."Km"); } ?></td>
                    <td class="editavel text-center"><?php if($row['SecondFrete'] == 0) {echo('-'); } else { echo(str_replace('.',',',$row['SecondFrete'])); } ?></td>
                    <td class="editavel text-center"><?php if($row['SecondKm'] == 0) {echo('-'); } else { echo("".$row['SecondKm']."Km"); } ?></td>
                    <td class="editavel text-center"><?php if($row['ThirdFrete'] == 0) {echo('-'); } else { echo(str_replace('.',',',$row['ThirdFrete'])); } ?></td>
                    <td class="editavel text-center"><?php if($row['ThirdKm'] == 0) {echo('-'); } else { echo("".$row['ThirdKm']."Km"); } ?></td>
                    <td class="editavel text-center"><?php if($row['FourthFrete'] == 0) {echo('-'); } else { echo(str_replace('.',',',$row['FourthFrete'])); } ?></td>
                    <td class="editavel text-center"><?php if($row['FourthKm'] == 0) {echo('-'); } else { echo("".$row['FourthKm']."Km"); } ?></td>
                    <td class="editavel text-center"><?php if($row['FifthFrete'] == 0) {echo('-'); } else { echo(str_replace('.',',',$row['FifthFrete'])); } ?></td>
                    <td class="editavel text-center"><?php if($row['FifthKm'] == 0) {echo('-'); } else { echo("".$row['FifthKm']."Km"); } ?></td>
                    <td class="text-center"><?php if($row['mediaTKU'] == 0) {echo('-'); } else { echo(str_replace('.',',',$row['mediaTKU'])); } ?></td>

                </tr>
            <?php
                        }
                    }
                }
            ?>
        </tbody>
    </table>
</div>

        <script type="text/javascript">
            $(function(){
            $('#Atualizar').click(function(){
                $('#tbleditavelfreteatual').find('tr').each(function () {
                    var $tds = $(this).find('td');                     
                    
                    var datinha=document.getElementById('dataTabelas').value;
                    var data = {
                        id: $tds.eq(0).text(),
                        freteVal1: $tds.eq(2).text().replace(',','.'),
                        freteKm1: $tds.eq(3).text().replace(',','.'),
                        freteVal2: $tds.eq(4).text().replace(',','.'),
                        freteKm2: $tds.eq(5).text().replace(',','.'),
                        freteVal3: $tds.eq(6).text().replace(',','.'),
                        freteKm3: $tds.eq(7).text().replace(',','.'),
                        freteVal4: $tds.eq(8).text().replace(',','.'),
                        freteKm4: $tds.eq(9).text().replace(',','.'),
                        freteVal5: $tds.eq(10).text().replace(',','.'),
                        freteKm5: $tds.eq(11).text().replace(',','.'),
                        datinha:datinha 
                    };
                    
                    $.ajax({
                        url:'service/freteAtualService.php',
                        type:'POST',
                        data: data,
                        success:function(data){
                            $("#msgSubmit").removeClass().addClass('alert alert-success');
                            $('#msgSubmit').delay(1000).fadeOut();
                            setTimeout(location.reload.bind(location), 2000);
                        }, error: function() {
                            $("#msgSubmitErro").removeClass().addClass('alert alert-danger');
                            $('#msgSubmitErro').delay(1000).fadeOut();
                        }
                    })
            })
        })
    });
        </script>