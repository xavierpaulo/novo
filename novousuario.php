<?php
session_start();
if(!empty($_SESSION['id'])){
    echo "";
    echo "";
}else{
    $_SESSION['msg'] = "Área restrita";
    header("Location: login.php");  
}
?> 

<?php
ob_start();
$btnCadUsuario = filter_input(INPUT_POST, 'btnCadUsuario', FILTER_SANITIZE_STRING);
if($btnCadUsuario){
    include_once 'conexao.php';
    $dados_rc = filter_input_array(INPUT_POST, FILTER_DEFAULT);

    $erro = false;

    $dados_st = array_map('strip_tags', $dados_rc);
    $dados = array_map('trim', $dados_st);

    if(in_array('',$dados)){
        $erro = true;
        $_SESSION['msg'] = "Necessário preencher todos os campos";
    }elseif((strlen($dados['senha'])) < 6){
        $erro = true;
        $_SESSION['msg'] = "A senha deve ter no minímo 6 caracteres";
    }elseif(stristr($dados['senha'], "'")) {
        $erro = true;
        $_SESSION['msg'] = "Caracter ( ' ) utilizado na senha é inválido";
    }else if(filter_var($dados['email'], FILTER_VALIDATE_EMAIL)==FALSE){
        $erro = true;
        $_SESSION['msg']="Formato de e-mail Inválido "; 

    }else{
        $result_usuario = "SELECT id FROM usuarios WHERE usuario='". $dados['usuario'] ."'";
        $resultado_usuario = mysqli_query($conn, $result_usuario);
        if(($resultado_usuario) AND ($resultado_usuario->num_rows != 0)){
            $erro = true;
            $_SESSION['msg'] = "Este usuário já está sendo utilizado";
        }

        $result_usuario = "SELECT id FROM usuarios WHERE email='". $dados['email'] ."'";
        $resultado_usuario = mysqli_query($conn, $result_usuario);
        if(($resultado_usuario) AND ($resultado_usuario->num_rows != 0)){
            $erro = true;
            $_SESSION['msg'] = "Este e-mail já está cadastrado";
        }
    }


        //var_dump($dados);
    if(!$erro){
            //var_dump($dados);
        $dados['senha'] = password_hash($dados['senha'], PASSWORD_DEFAULT);

        $result_usuario = "INSERT INTO usuarios (nome, email, usuario, senha) VALUES (
        '" .$dados['nome']. "',
        '" .$dados['email']. "',
        '" .$dados['usuario']. "',
        '" .$dados['senha']. "'
    )";
    $resultado_usario = mysqli_query($conn, $result_usuario);
    if(mysqli_insert_id($conn)){
        $_SESSION['msgcad'] = "Usuário cadastrado com sucesso";
        header("Location: login.php");
    }else{
        $_SESSION['msg'] = "Erro ao cadastrar o usuário";
    }
}

}
?>

<!doctype html>
<html lang="en">
<head>
   <meta charset="utf-8" />
   <link rel="icon" type="image/png" href="assets/img/favicon.ico">
   <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

   <title>Meu Preço Certo V1.</title>

   <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
   <meta name="viewport" content="width=device-width" />


   <!-- Bootstrap core CSS     -->
   <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

   <!-- Animation library for notifications   -->
   <link href="assets/css/animate.min.css" rel="stylesheet"/>

   <!--  Light Bootstrap Table core CSS    -->
   <link href="assets/css/light-bootstrap-dashboard.css?v=1.4.0" rel="stylesheet"/>


   <!--  CSS for Demo Purpose, don't include it in your project     -->
   <link href="assets/css/demo.css" rel="stylesheet" />


   <!--     Fonts and icons     -->
   <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
   <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
   <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />
</head>
<body>



    <div class="wrapper">
        <div class="sidebar" data-color="purple" data-image="assets/img/sidebar-5.jpg">

            <!--   you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple" -->


            <div class="sidebar-wrapper">
                <div class="logo">
                    <img src="images/EA-Logo.png" class="img-foot" alt="Sega" title="Sega" width="200" height="60">

                </a>
            </div>

            <ul class="nav">
                <li class="active">
                    <a href="dashboard.php">
                        <i class="pe-7s-graph"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <li>
                    <a href="usuario.php">
                        <i class="pe-7s-user"></i>
                        <p>Usuário</p>
                    </a>
                </li>
                <li>
                    <a href="novousuario.php">
                        <i class="pe-7s-user"></i>
                        <p>Cadastrar Usuário</p>
                    </a>
                </li>
                <li>
                    <a href="table.php">
                        <i class="pe-7s-note2"></i>
                        <p>Tabelas</p>
                    </a>
                </li>



            </ul>
        </div>
    </div>

    <div class="main-panel">
        <nav class="navbar navbar-default navbar-fixed">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="dashboard.php">Dashboard</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-left">


                        <li>
                           <a href="">
                            <i class="fa fa-sair"></i>
                            <p class="hidden-lg hidden-md">

                            </p>
                        </a>
                    </li>


                </ul>

                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <li>
                            <a href="">
                                <?php
                                if(!empty($_SESSION['id'])){
                                  echo "Olá ".$_SESSION['nome'].", Bem vindo(a) <br>";
                              }
                              ?>
                          </a>
                      </li>
                  </li>
                  <li>
                    <a href="sair.php">
                        <p>Sair</p>
                    </a>
                </li>
                <li class="separator hidden-lg"></li>
            </ul>
        </div>
    </div>
</nav>
<!--Aqui começa o cadastro-->
<div class="content">
    <div class="container-fluid">
        <div class="form-signin" style="background: #ffffff;">
            <br><br>
            <center><h2>Cadastrar Usuário</h2></center>
            <b><?php
            if(isset($_SESSION['msg'])){
                echo $_SESSION['msg'];
                unset($_SESSION['msg']);
            }
            ?></b>
            <center>
                <div class="container">
                    <form  method="POST" action="">
                        <!--<label>Nome</label>-->
                        <center>
                            <div class="col-md-2"></div>
                            <div class="col-md-9">
                                <input class="form-control" type="text" name="nome" placeholder="Digite o nome e o sobrenome" class="form-control">
                                <br>
                            </div>
                            <!--<label>E-mail</label>-->
                            <center>
                                <div class="col-md-2"></div>
                                <div class="col-md-9">
                                    <input class="form-control" type="text" name="email" placeholder="Digite o seu e-mail" class="form-control"><br>
                                </div>
                                <!--<label>Usuário</label>-->
                                <center>
                                    <div class="col-md-2"></div>
                                    <div class="col-md-9">
                                        <input class="form-control" type="text" name="usuario" placeholder="Digite o usuário" class="form-control"><br>
                                    </div>
                                    <!--<label>Senha</label>-->
                                    <center>
                                        <div class="col-md-2"></div>
                                        <div class="col-md-9">
                                            <input class="form-control" type="password" name="senha" placeholder="Digite a senha" class="form-control"><br>
                                        </div>
                                        <div class="col-md-12">
                                            <input type="submit" name="btnCadUsuario" value="Cadastrar" class="btn btn-success"><br><br>
                                        </div>
                                        <div class="row text-center" style="margin-top: 20px;"> 
                                        </div>
                                    </div>
                                    <br><br><br><br><br><br><br><br>
                                </form>
                            </div>
                        </div>
                    </div>



                    <footer class="footer">
                        <div class="container-fluid">
                  <!--  <nav class="pull-left">
                        <ul>
                            <li>
                                <a href="#">
                                    Home
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Company
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Portfolio
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                   Blog
                                </a>
                            </li>
                        </ul>
                    </nav> -->
                    <p class="copyright pull-right">
                        &copy; <script>document.write(new Date().getFullYear())</script> <a href="#">Paulo Arthur</a>
                    </p>
                </div>
            </footer>

        </div>
    </div>


</body>

<!--   Core JS Files   -->
<script src="assets/js/jquery.3.2.1.min.js" type="text/javascript"></script>
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

<!--  Charts Plugin -->
<script src="assets/js/chartist.min.js"></script>

<!--  Notifications Plugin    -->
<script src="assets/js/bootstrap-notify.js"></script>

<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>

<!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
<script src="assets/js/light-bootstrap-dashboard.js?v=1.4.0"></script>

<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
<script src="assets/js/demo.js"></script>

</html>
