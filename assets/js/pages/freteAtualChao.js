$(document).ready(function(){
	$('#tbleditavelfreteatualchao tbody tr td.editavel').each(function(){
			$(this).click(function(){
			var exists=$(this).closest('tr').next().length;
			var index=$(this).index();
			var separar=$(this).parent('tr').find('td:eq(1)').text();
			var spliter=separar.split("-");
			spliter[0]=parseInt(spliter[0],10);
			spliter[1] = parseInt(spliter[1],10);
			var conteudoOriginal= $(this).text();
			var novoElemento= $('<input/>',{ type:'text', value:conteudoOriginal, class:'text-center', style: 'width: 48px; height: 23px;'});
			$(this).html(novoElemento.bind('blur keydown',function(e){
				var keyCode=e.which;
				var conteudoNovo= $(this).val();
				if(conteudoNovo=="" || conteudoNovo<0){
					conteudoNovo="-";
				}
				
				if(keyCode==13 && conteudoNovo !=conteudoOriginal){
					if(index % 2 !== 0){
						if((conteudoNovo < spliter[0]) || (conteudoNovo > spliter[1])){
							$(this).parent().html(conteudoOriginal).css("background-color" ,"#ff8566");
							alert("Valor inválido");
						}else{
							if(exists==0){
							$(this).parents().children('tr:nth-child(1)').children('td:nth-child('+(index+2)+')').trigger('click');
						}else{
							$(this).parent().html(conteudoNovo).css("background-color","").closest('tr').next().children('td:nth-child('+(index+1)+')').trigger('click');
						}
						}
		
					}else {
						if(exists==0){
							$(this).parent().html(conteudoNovo).parents().children('tr:nth-child(1)').children('td:nth-child('+(index+2)+')').trigger('click');
						}else{
							$(this).parent().html(conteudoNovo).closest('tr').next().children('td:nth-child('+(index+1)+')').trigger('click');
						}
						
					}
					
			}else if(keyCode==27 || e.type=='blur'){
				$(this).parent().html(conteudoNovo);
			}
			}));
			$(this).children().select();
		})
	});
})